package com.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.Message;
import com.model.Item;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

@Controller
public class MessageController {
    private ObjectMapper jackson = new ObjectMapper();

    @MessageMapping("/message")
    @SendTo("/topic/items")
    public Item item(Message message) throws InterruptedException, JsonProcessingException {
        Thread.sleep(1000);
        String content = jackson.writeValueAsString(message);
        return new Item(HtmlUtils.htmlEscape(content));
    }
}
