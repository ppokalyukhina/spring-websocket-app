package com.model;

public class Item {
    private String content;

    public Item() {
    }

    public Item(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
