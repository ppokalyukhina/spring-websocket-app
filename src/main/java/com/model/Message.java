package com.model;

public class Message {
    private String name;
    private int id;

    public Message() {
    }

    public Message(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
